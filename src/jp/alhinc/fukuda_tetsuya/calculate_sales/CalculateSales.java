package jp.alhinc.fukuda_tetsuya.calculate_sales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UTFDataFormatException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.zip.DataFormatException;


public class CalculateSales {

	public static void main(String[] args) throws Exception {

	try {

		// rcdファイルのみを取得

			File folder = new File("C:\\Users\\福田哲也\\workspace\\売り上げ集計課題");
			File[] files = folder.listFiles();
			ArrayList<File> readFiles = new ArrayList <File>();
			int nameNumber[] = new int[100];
			try {
				if (files != null) {

					 for (int i = 0; i < files.length; i++) {
				        String fileName = files[i].getName();

				        if(fileName.matches("[0-9]{8}.rcd")) {
				        	readFiles.add(files[i]);

				        	String[] setArray = new String[2];
				        	setArray = fileName.split("\\.");
				        	nameNumber[i] = Integer.parseInt(setArray[0]);
				        	if(i != 0 && Math.abs(nameNumber[i] - nameNumber[i - 1]) != 1) {
				        		throw new IOException();
				        	}
				        } else if (fileName.matches("branch.lst") || fileName.matches("branch.out")) {
				        	continue;
				        } else {
				        	throw new DataFormatException();
				        }
					  }
				  }
			}catch (DataFormatException e) {
				System.out.println("支店定義ファイルが存在しません");
				System.exit(0);
			}catch (IOException e) {
				System.out.println("売上ファイル名が連番になっていません");
				System.exit(0);
			}

		// 全rcdファイルを一つの配列shopArrayに格納
		// 売り上げファイルと支店定義ファイルを比較するためのハッシュマップcompareMapを用意

			 ArrayList shopArray = new ArrayList();
			 HashMap<String, String> compareMap = new HashMap<String, String>();

				 BufferedReader br = null;
				 for(File readFile : readFiles) {

					 String fileName = readFile.getName();
					 File file = new File(args[0], fileName);
					 FileReader fr = new FileReader(file);
					 br = new BufferedReader(fr);

					 try {
						 String line = null;
						 int whileCount = 0;
						 while((line = br.readLine()) != null) {
							 whileCount++;
							 if(whileCount == 3) {
								 throw new DataFormatException();
							 } else if(line.matches("[0-9]{0,9}")) {
								 shopArray.add(line);
								 if(line.matches("[0-9]{3}")) {
									 compareMap.put(fileName, line); //支店名と支店コードはcompareMapへ格納
								 } else if(line.matches("^[1-9][0-9]{3,8}")) {
									 continue;
								 } else {
									 throw new UTFDataFormatException(fileName);
								 }
							 } else {
								 throw new IOException();
							 }
						 }
					 } catch (DataFormatException e) {
						System.out.println(fileName + "のフォーマットが不正です");
						br.close();
						System.exit(0);
					 } catch (UTFDataFormatException e) {
						 System.out.println("予期せぬエラーが発生しました");
						 System.out.println(fileName + "の売上金額に数字以外が含まれていないかご確認ください");
						 br.close();
						 System.exit(0);
					 } catch (IOException e) {
						 System.out.println("予期せぬエラーが発生しました");
						 br.close();
						 System.exit(0);
					 } finally {
						 br.close();
						 // break;	//「finally ブロックは正常に完了しません」と警告が表示されてしまい、危ない予感
					 }
				 }

		//配列shopArrayを拡張for(ローカル変数shopDate)を用いて、ハッシュマップshopMapに整理

			 HashMap<String, Integer> shopMap = new HashMap<String, Integer>();
			 int numberCount = 0;
			 int saleCount = 0;
			 String shopNumbers[] = new String[10];
			 int shopSales[] = new int[10];
			 for(Object shopDate : shopArray) {
				 String shopStr = String.valueOf(shopDate);

				 if(shopStr.matches("[0-9]{3}")) {
					shopNumbers[numberCount] = shopStr;
					numberCount++;
				 } else {
					 shopSales[saleCount] = Integer.parseInt(shopStr);
					 saleCount++;
				 }
			 }

			 try {
				 for(int i = 0; i < shopSales.length - 1; i++) {
					if(shopMap.containsKey(shopNumbers[i])) {
						int shopSum = 0;
						shopSum = shopMap.get(shopNumbers[i]) + shopSales[i];
						if(shopSum <1000000000) {
							shopMap.put(shopNumbers[i], shopSum);
						} else if(shopSum >=1000000000) {
							throw new ArithmeticException();
						} else  {
							throw new IOException();
						}
					} else {
						shopMap.put(shopNumbers[i], shopSales[i]);
					}
				 }
			 } catch(ArithmeticException e) {
				System.out.println("合計金額が10桁を超えました");
				System.exit(0);
			 } catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				System.exit(0);
			 }



		// branch.lstの処理

			 File file = new File("C:\\Users\\福田哲也\\workspace\\売り上げ集計課題\\branch.lst");
			 FileReader frLst = new FileReader(file);
			 BufferedReader brLst = new BufferedReader(frLst);

			 String[][] shopList = new String[10][];
			 String lineLst = null;
			 int index = 0;

			 try {
				 while((lineLst = brLst.readLine()) != null) {
					 if(lineLst.matches("[0-9]{3},.+支店$")) {

						 String[] tmpList = new String[2];
						 tmpList = lineLst.split(",");

						 for (String key : compareMap.keySet()) {
							 if(compareMap.get(key).equals(tmpList[0])) {
								 shopList[index] = tmpList;
								 break;
							 } else if(compareMap.get(key) != tmpList[0]) {
								 continue;
							 } else {
								 throw new UTFDataFormatException(key);
							 }
						 }
						 index++;
					 }
					 else {
						 throw new DataFormatException();
					 }
				 }
			 } catch (DataFormatException e) {
				System.out.println("支店定義ファイルのフォーマットが不正です");
				brLst.close();
				System.exit(0);
			 } catch (UTFDataFormatException key) {
				System.out.println(key + "の支店コードが不正です");
				brLst.close();
				System.exit(0);
			 }finally {
				brLst.close();
			 }

		// 支店別集計ファイルへの出力

			 FileWriter fw = new FileWriter("C:\\Users\\福田哲也\\workspace\\売り上げ集計課題\\branch.out", true);
			 for(int i = 0; i < shopList.length; i++) {
				 fw.write(shopList[i][0]);
				 fw.write(",");
				 fw.write(shopList[i][1]);
				 fw.write(",");
				 fw.write(shopMap.get(shopList[i][0]) + "\n");
				 fw.flush();
			 }
			 fw.close();

		} catch(IllegalArgumentException e) {
			System.out.println("予期せぬエラーが発生しました");
		} finally {
			System.exit(0);
		}
	}
}
